<?php

require_once 'model/records.php';

class MakerController{
    private $model;

    public function __CONSTRUCT(){
        $this->model = new Records();
    }

    public function Index(){
        require_once 'view/header.php';
        require_once 'view/maker/index.php';
        require_once 'view/footer.php';
    }

    public function Manual(){
        require_once 'view/header.php';
        require_once 'view/maker/manual.php';
        require_once 'view/footer.php';
    }

    public function Register() {
        require_once 'view/header.php';
        require_once 'view/maker/register.php';
        require_once 'view/footer.php';
    }

    public function Upload() {
        $upload_comic = $this->uploadComic();
        if ($upload_comic['result']) {
            $record = new Records();

            $record->name = $_REQUEST['name'];
            $record->age = $_REQUEST['age'];
            $record->grade = $_REQUEST['grade'];
            $record->school = $_REQUEST['school'];
            $record->comic = $upload_comic['file_name'];

            if($this->model->create($record)){
                header('Location: index.php?c=maker&upload=true');
                die;
            } else {
                unlink('assets/images/comics/' . $upload_comic['file_name']);
            }
        }

        header('Location: index.php?c=maker&upload=false');
        die;
    }

    public function Mural() {
        $records = $this->model->getList();


        require_once 'view/header.php';
        require_once 'view/maker/mural.php';
        require_once 'view/footer.php';
    }

    public function Inspiration() {
        require_once 'view/header.php';
        require_once 'view/maker/inspiration.php';
        require_once 'view/footer.php';
    }

    public function Comic() {
        $record = $this->model->getById($_REQUEST['comic']);

        require_once 'view/header.php';
        require_once 'view/maker/comic.php';
        require_once 'view/footer.php';
    }

    private function uploadComic() {
        $result = false;
        $file_name = '';
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (!empty($_FILES)){
                if (array_key_exists('comic', $_FILES)) {
                    if (!file_exists('assets/images/comics')) {
                        mkdir('assets/images/comics', 0777, true);
                    }

                    $dir = 'assets/images/comics/';
                    $file_name = uniqid() . "_" . basename($_FILES['comic']['name']);
                    $uploaded_file = $dir . $file_name;
                    if (move_uploaded_file($_FILES['comic']['tmp_name'], $uploaded_file)) {
                        $result = true;
                    }
                }
            }
        }

        return ['result' => $result, 'file_name' => $file_name];
    }
}