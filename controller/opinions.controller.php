<?php

class OpinionsController{
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/opinions/index.php';
        require_once 'view/footer.php';
    }
    public function Libertad(){
        require_once 'view/header.php';
        require_once 'view/opinions/libertad.php';
        require_once 'view/footer.php';
    }
    public function Inclusion(){
        require_once 'view/header.php';
        require_once 'view/opinions/inclusion.php';
        require_once 'view/footer.php';
    }
    public function Igualdad(){
        require_once 'view/header.php';
        require_once 'view/opinions/igualdad.php';
        require_once 'view/footer.php';
    }
    public function Honestidad(){
        require_once 'view/header.php';
        require_once 'view/opinions/honestidad.php';
        require_once 'view/footer.php';
    }
    public function Justicia(){
        require_once 'view/header.php';
        require_once 'view/opinions/Justicia.php';
        require_once 'view/footer.php';
        
    }
     public function Respeto(){
        require_once 'view/header.php';
        require_once 'view/opinions/Respeto.php';
        require_once 'view/footer.php';
       
    }
    public function Empatia(){
        require_once 'view/header.php';
        require_once 'view/opinions/empatia.php';
        require_once 'view/footer.php';
    }
    public function CulturaPaz(){
        require_once 'view/header.php';
        require_once 'view/opinions/culturapaz.php';
        require_once 'view/footer.php';
    }
    
}