<?php

class Records
{
    private $pdo;

    public $id;
    public $name;
    public $age;
    public $grade;
    public $school;
    public $comic;
    public $created_at;

    public function __CONSTRUCT()
    {
        try {
            $this->pdo = Database::StartUp();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function getList()
    {
        try {
            $result = array();

            $stm = $this->pdo->prepare("SELECT * FROM records");
            $stm->execute();

            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function getById($id)
    {
        try {
            $stm = $this->pdo
                ->prepare("SELECT * FROM records WHERE id = ?");


            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function deleteById($id)
    {
        try {
            $stm = $this->pdo
                ->prepare("DELETE FROM records WHERE id = ?");

            $stm->execute(array($id));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function updateById($data)
    {
        try {
            $sql = "UPDATE alumnos SET 
						name          = ?, 
						age        = ?,
                        grade        = ?,
						school            = ?, 
						comic = ?
				    WHERE id = ?";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->name,
                        $data->age,
                        $data->grade,
                        $data->school,
                        $data->comic,
                        $data->id
                    )
                );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function create(Records $data)
    {
        try {
            $sql = "INSERT INTO records (name,age,grade,school,comic,created_at) 
		        VALUES (?, ?, ?, ?, ?, ?)";

            $this->pdo->prepare($sql)
                ->execute(
                    array(
                        $data->name,
                        $data->age,
                        $data->grade,
                        $data->school,
                        $data->comic,
                        date('Y-m-d')
                    )
                );
            return true;
        } catch (Exception $e) {
            return false;
            //die($e->getMessage());
        }
    }
}