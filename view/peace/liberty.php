<style>
    body {
        background-image: linear-gradient(rgba(255, 255, 255, 0.7), rgba(255, 255, 255, 0.7)), url("assets/images/bg_hive.png");
    }
</style>

<div class="sticky-top">
    <a href="?c=peace">
    <span class="fa-stack fa-2x m-2">
        <i class="fas fa-square fa-stack-2x text-danger"></i>
        <i class="fas fa-arrow-left fa-stack-1x fa-inverse"></i>
    </span>
    </a>
    <a href="?c=home">
    <span class="fa-stack fa-2x">
        <i class="fas fa-square fa-stack-2x text-info"></i>
        <i class="fas fa-home fa-stack-1x fa-inverse"></i>
    </span>
    </a>
</div>

<div class="container">
    <div class="row  mt-5">
        <div class="col col-lg-8 ">
            <div class="jumbotron  animate__animated animate__flipInX">
                <h1 class="display-4 animate__animated animate__zoomInDown animate__delay-1s">
                    <span class="badge badge-warning">Libertad</span>
                </h1>
                <p class="lead animate__animated animate__flipInY animate__delay-1s">
                    Es el derecho que todos los seres vivos poseemos para desarrollar ideas, actividades y proyectos;
                    alcanzar metas, practicar una fe, sin afectar negativamente a quienes estén a nuestro alrededo
                </p>
            </div>
        </div>
        <div class="col text-center">
            <img class="animate__animated animate__pulse animate__infinite" style="height: 350px"
                 src="assets/images/eirene.png">
        </div>
    </div>
</div>

<div class="fixed-bottom">
    <a href="?c=home" class="float-right">
    <span class="fa-stack fa-2x m-2">
        <i class="fas fa-square fa-stack-2x text-success"></i>
        <i class="fas fa-arrow-right fa-stack-1x fa-inverse"></i>
    </span>
    </a>
</div>
