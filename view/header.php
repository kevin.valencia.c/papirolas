<!doctype html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <!-- externals css -->
    <link rel="stylesheet" href="assets/css/style.css">
    <!-- Font Awesome -->
    <script src="assets/js/font_awesome.js"></script>
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Knewave&family=Raleway:wght@400;600&display=swap" rel="stylesheet">


    <title>Papirolas</title>
</head>
<body>