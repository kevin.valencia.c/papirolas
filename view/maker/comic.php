<style>
    html, body {
        height: 100%;
    }

    body {
        background-image: url("assets/images/TexturaColmenaClaro.jpg");
    }
</style>

<div class="sticky-top">
    <a href="?c=maker&a=mural">
    <span class="fa-stack fa-2x m-2">
        <i class="fas fa-square fa-stack-2x text-danger"></i>
        <i class="fas fa-arrow-left fa-stack-1x fa-inverse"></i>
    </span>
    </a>
    <a href="?c=home">
    <span class="fa-stack fa-2x">
        <i class="fas fa-square fa-stack-2x text-info"></i>
        <i class="fas fa-home fa-stack-1x fa-inverse"></i>
    </span>
    </a>
</div>

<div class="container">
    <div class="row  mt-5 justify-content-center">
        <div class="col col-lg-6">
            <div class="jumbotron animate__animated animate__flipInX">
                <h1 class=" animate__animated animate__zoomInDown animate__delay-1s">
                    <?= $record->name; ?>
                    <span class="badge badge-info"><?= $record->grade; ?>°</span>
                </h1>
                <p class="lead animate__animated animate__flipInY animate__delay-1s">
                    Escuela: <?= $record->school; ?>
                </p>
                <p class="lead animate__animated animate__flipInY animate__delay-1s">
                    Edad: <?= $record->age; ?>
                </p>
            </div>
        </div>
    </div>
    <div class="row  mb-5">
        <div class="col text-center">
            <img class="animate__animated animate__pulse" style="height: 350px"
                 src="assets/images/comics/<?= $record->comic; ?>">
        </div>
    </div>
</div>