<style>
    html,
    body {
        height: 100%;
    }

    body {
        background-image: url("assets/images/TexturaColmenaClaro.jpg");
    }
</style>

<div class="sticky-top">
    <a href="?c=Maker">
        <img src="assets\images\Regresar boton.png" alt="Back" width="72" height="72" title="Atras">
    </a>
    <a href="?c=home">
        <img src="assets\images\Home boton.png" alt="Home" width="72" height="72" title="Inicio">
    </a>
</div>

<div class="container">
    <div class="row  mt-5">
        <div class="col col-lg-8">
            <div class="jumbotron animate__animated animate__flipInX">
                <h1 class="display-4 animate__animated animate__zoomInDown animate__delay-1s">
                    <span class="badge badge-primary">Comparte tu cómic</span>
                </h1>
                <p class="lead animate__animated animate__flipInY animate__delay-1s">
                    Comparte tu cómic para que demás niñas y niños puedan verlo. También puedes compartirlo en Instagram
                    o FB.
                </p>
                <p class="lead animate__animated animate__flipInY animate__delay-1s">
                    No olvides poner #UDGVirtualPapirolas #ElFestivalDeLaPaz
                </p>

                <form action="?c=maker&a=upload" method="post" enctype="multipart/form-data">
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" required class="form-control" id="name" placeholder="Escribe tu nombre">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="age" class="col-sm-2 col-form-label">Edad</label>
                        <div class="col-sm-10">
                            <input type="number" name="age" required class="form-control" id="age" placeholder="Escribe tu edad">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="grade" class="col-sm-2 col-form-label">Grado</label>
                        <div class="col-sm-10">
                            <select name="grade" class="custom-select" id="grade" required>
                                <option value="">Selecciona tu grado</option>
                                <option value="1">Primero</option>
                                <option value="2">Segundo</option>
                                <option value="3">Tercero</option>
                                <option value="4">Cuarto</option>
                                <option value="5">Quinto</option>
                                <option value="6">Sexto</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="school" class="col-sm-2 col-form-label">Escuela</label>
                        <div class="col-sm-10">
                            <input type="text" name="school" required class="form-control" id="school" placeholder="Escribe el nombre de la escuela">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="comic" class="col-sm-2 col-form-label">Cómic</label>
                        <div class="col-sm-10">
                            <div class="custom-file">
                                <input type="file" name="comic" class="custom-file-input" id="colic">
                                <label class="custom-file-label" for="comic">Sube tu cómic</label>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form>
            </div>
        </div>
        <div class="col text-center">
            <img class="animate__animated animate__pulse animate__infinite" style="height: 350px" src="assets/images/eirene.png">
        </div>
    </div>
</div>