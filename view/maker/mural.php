<style>
    html,
    body {
        height: 100%;
    }

    body {
        background-image: url("assets/images/TexturaColmenaClaro.jpg");
    }
</style>

<div class="sticky-top">
    <a href="?c=Maker">
        <img src="assets\images\Regresar boton.png" alt="Back" width="72" height="72" title="Atras">
    </a>
    <a href="?c=home">
        <img src="assets\images\Home boton.png" alt="Home" width="72" height="72" title="Inicio">
    </a>
</div>

<div class="container">
    <div class="row  mt-5">
        <div class="col col-lg-8">
            <div class="jumbotron animate__animated animate__flipInX">
                <h1 class="display-4 animate__animated animate__zoomInDown animate__delay-1s manual_title">
                    <span class="badge">Observa a la comunidad</span>
                </h1>
                <p class="lead animate__animated animate__flipInY animate__delay-1s manual_description">
                    Ve todos los comics que han creado más niñas y niños sobre la cultura de paz. ¿Cuál te gusta más?
                </p>
            </div>
        </div>
        <div class="col text-center">
            <img class="animate__animated animate__pulse animate__infinite" style="height: 350px" src="assets/images/eirene.png">
        </div>
    </div>

    <div class="row text-center mt-5 mb-5">
        <?php foreach ($records as $record) : ?>
            <div class="col mb-5 col-lg-3">
                <a href="?c=maker&a=comic&comic=<?= $record->id; ?>">
                    <img class="bg-info border border-white rounded p-3 animate__animated animate__pulse animate__infinite" style="max-height: 200px; max-width: 200px" src="assets/images/comics/<?= $record->comic; ?>">
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>