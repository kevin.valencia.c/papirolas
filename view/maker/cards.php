<style>
    body {
        background-image: url("assets/images/TexturaColmenaClaro.jpg");
    }
</style>

<?php
$image = isset($_REQUEST['situation']) ?  $_REQUEST['situation'] : "horror";

$nextpage = $_REQUEST['situation'] == '8' ? 1 : intval($_REQUEST['situation']) + 1;

$text;

switch ($_REQUEST['situation']) {
    case 1:
        $text = "una familia discute en casa por la elección del canal de televisión ¿Cómo lo resolverias?";
        break;
    case 2:
        $text = "LLega una compañera nueva a clases y no quiren jugar con ella ¿Cómo le ayudarias?";
        break;
    case 3:
        $text = "En la ciudad hay personas con discapacidad que encuentran en su camino grandes barreras que impiden ser incluido ¿Tú qué harías para ayudarlos?";
        break;
    case 4:
        $text = "Otro jugadores siempre se burlan de un amigo en un videojuego ¿Qué sugieres realizar para que dejen de hacerlo?";
        break;
    case 5:
        $text = "han tirado básura en tu parque favorito ¿Cómo lo resolverias?";
        break;
    case 6:
        $text = "Se organizó un gran interplanetario pero nadie se entiende ¿Cómo lo solucionarías?";
        break;
    case 7:
        $text = "En la casa hay un fantasma que hace mucho ruidoy no deja dormir a las personas ¿Qué harías para que pudieran vivir en armonía?";
        break;
    case 8:
        $text = "Los animales de la playa están en riesgo por la basura que dejan las personas ¿Cómo lo resolverias?";
        break;
}

?>

<div class="sticky-top">
    <a href="?c=Maker&a=Inspiration">
        <img src="assets\images\Regresar boton.png" alt="Back" width="72" height="72" title="Atras">
    </a>
    <a href="?c=home">
        <img src="assets\images\Home boton.png" alt="Home" width="72" height="72" title="Inicio">
    </a>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col col-lg-6">
            <div class="card bg-transparent no_border">
                <img class="img_size" src="assets/images/Creadores/tarjetasInformacion/<?php echo $image ?>icono.png" alt="inclusión">
            </div>
        </div>
        <div class="col col-lg-6 mt-5">
            <div class="frame_text mt-5">
                <div class="text_card"><?php echo $text ?></div>
            </div>
        </div>
    </div>
</div>

<div class="fixed-bottom">
    <a href="?c=inspiration&situation=<?php echo $nextpage ?>" class="float-right">
        <img src="assets\images\boton adelante.png" alt="Next" width="72" height="72" title="Siguiente">
    </a>
</div>