<style>
    body {
        background-image: url("assets/images/TexturaColmenaClaro.jpg");
    }
</style>

<div class="sticky-top button_container">
    <a href="?c=Maker">
        <img src="assets\images\Regresar boton.png" alt="Back" width="72" height="72" title="Atras">
    </a>
    <a href="?c=home">
        <img src="assets\images\Home boton.png" alt="Home" width="72" height="72" title="Inicio">
    </a>
</div>

<div class="container">
    <div class="row mb-5">
        <div class="col offset-lg-2 col-2 col-lg-2 ">
            <a href="https://www.makebeliefscomix.com/" target="_blank">
                <div class="img-overlay-wrap">
                    <img class="img-overlay-wrap-svg" src="assets/images/boton5.png" alt="crear comic">
                    <span class="description_panel">Tarjeta de situación</span>
                </div>
            </a>
        </div>
        <div class="symbols">
            <span>+</span>
        </div>
        <div class="col col-2 col-lg-2 no_p_m">
            <a href="https://www.makebeliefscomix.com/" target="_blank">
                <div class="img-overlay-wrap">
                    <img class="img-overlay-wrap-svg" src="assets/images/boton5.png" alt="crear comic">
                    <span class="description_panel">Escoge un valor</span>
                </div>
            </a>
        </div>
        <div class="symbols">
            <span>=</span>
        </div>
        <div class="col col-2 col-lg-2 no_p_m">
            <a href="https://www.makebeliefscomix.com/" target="_blank">
                <div class="img-overlay-wrap">
                    <img class="img-overlay-wrap-svg" src="assets/images/Creadores/creacionComic.png" alt="crear comic">
                    <span class="crear_comic"> Crea tu cómic Aquí</span>
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col offset-lg-4 col-lg-3">
            <a href="?c=opinions">
                <div class="img-overlay-wrap">
                    <img class="img-overlay-wrap-png" src="assets/images/Creadores/tarjetasInformacion/Valores.png" alt="situacion">
                </div>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col col-lg-2">
            <div class="animate__animated animate__flipInX">
                <div class="container_message">
                    <!-- <img class="icon_inspiration" src="assets/images/boton2.png" alt="boton">
                    <h1 class="display-6 title_message_inspiration animate__animated animate__zoomInDown animate__delay-1s">
                        <span class="badge">Crea tu cómic</span>
                    </h1>
                    <p class="lead animate__animated text_message_inspiration animate__flipInY animate__delay-1s">
                        Estás tarjetas te ayudarán a inspirarte a crear tu historia, averigua cada una de las situaciones y despúes crea tu cómic.
                    </p> -->
                </div>
            </div>
        </div>
        <!-- <div class=col>
            <img class="irene_message_inspiration animate__animated animate__pulse animate__infinite" style="height: 350px" src="assets/images/eirene.png">
        </div> -->
    </div>
    <div class="row">
        <div class="col">
            <div class="row text-center">
                <div class="col ">
                    <a href="?c=inspiration&situation=1">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-png" src="assets/images/Creadores/tarjetasInformacion/1icono.png" alt="situacion">
                        </div>
                    </a>
                </div>
                <div class="col ">
                    <a href="?c=inspiration&situation=2">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-png" src="assets/images/Creadores/tarjetasInformacion/2icono.png" alt="situacion">
                        </div>
                    </a>
                </div>
                <div class="col ">
                    <a href="?c=inspiration&situation=3">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-png" src="assets/images/Creadores/tarjetasInformacion/3icono.png" alt="situacion">
                        </div>
                    </a>
                </div>
                <div class="col ">
                    <a href="?c=inspiration&situation=4">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-png" src="assets/images/Creadores/tarjetasInformacion/4icono.png" alt="situacion">
                        </div>
                    </a>
                </div>
            </div>
            <div class="row mt-5 mb-5 text-center">
                <div class="col">
                    <a href="?c=inspiration&situation=5">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-png" src="assets/images/Creadores/tarjetasInformacion/5icono.png" alt="situacion">
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a href="?c=inspiration&situation=6">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-png" src="assets/images/Creadores/tarjetasInformacion/6icono.png" alt="situacion">
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a href="?c=inspiration&situation=7">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-png" src="assets/images/Creadores/tarjetasInformacion/7icono.png" alt="situacion">
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a href="?c=inspiration&situation=8">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-png" src="assets/images/Creadores/tarjetasInformacion/8icono.png" alt="situacion">
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>