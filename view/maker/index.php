<style>
    body {
        background-image: url("assets/images/TexturaColmenaClaro.jpg");
    }
</style>

<div class="sticky-top">
    <a href="?c=home">
        <img src="assets\images\Home boton.png" alt="Home" width="72" height="72" title="Inicio">
    </a>
</div>

<div class="container">
    <div class="row">
        <div class="col col-lg-6">
            <?php if (array_key_exists('upload', $_REQUEST)) : ?>
                <?php if ($_REQUEST['upload'] == 'true') : ?>
                    <h2 class="animate__animated animate__zoomInDown animate__delay-1s">
                        <span class="badge badge-info">Tu cómic se subió con éxito</span>
                    </h2>
                <?php endif; ?>
            <?php endif; ?>

            <div class="row text-center">
                <div class="col">
                    <a href="?c=maker&a=Inspiration">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-svg" src="assets/images/Creadores/inspiracion.png" alt="inspiración">
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a href="?c=maker&a=manual">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-svg" src="assets/images/Creadores/manual.png" alt="manual">
                        </div>
                    </a>
                </div>
            </div>
            <div class="row mt-5 mb-5 text-center">
                <div class="col">
                    <a href="?c=maker&a=register">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-svg" src="assets/images/Creadores/Subirarchivo.png" alt="Subir cómic">
                        </div>
                    </a>
                </div>

                <div class="col">
                    <a href="?c=maker&a=mural">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-svg" src="assets/images/Creadores/ComunidadBoton.png" alt="Comunidad">
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col offset-lg-1 col-lg-5">
            <div class="row">
                <div class="col col-lg-2">
                    <div class="animate__animated animate__flipInX">
                        <div class="container_message">
                            <img class="icon_creadores" src="assets/images/boton2.png" alt="boton">
                            <h1 class="display-6 title_message_creadores animate__animated animate__zoomInDown animate__delay-1s">
                                <span class="badge" style="font-size:100%; white-space:normal">Creadores</span>
                            </h1>
                            <p class="lead animate__animated text_message_creadores animate__flipInY animate__delay-1s">
                                Crea el contenido para divulgarlo a las colmenas de tu comunidad.
                            </p>
                            <ol class="lead list text_message_creadores">
                                <li>Haz tu cómic</li>
                                <li>La libreta del manual de uso</li>
                                <li>Sube tu cómic al mural</li>
                                <li>Mural de creaciones</li>
                            </ol>

                        </div>
                    </div>
                    <img class="irene_message animate__animated animate__pulse animate__infinite" style="height: 350px" src="assets/images/eirene.png">
                </div>
            </div>
        </div>
    </div>
</div>