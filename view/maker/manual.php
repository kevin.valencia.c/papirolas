<style>
    body {
        background-image: url("assets/images/TexturaColmenaClaro.jpg");
    }
</style>

<div class="sticky-top">
    <a href="?c=Maker">
        <img src="assets\images\Regresar boton.png" alt="Back" width="72" height="72" title="Atras">
    </a>
    <a href="?c=home">
        <img src="assets\images\Home boton.png" alt="Home" width="72" height="72" title="Inicio">
    </a>
</div>

<div class="container">
    <div class="row mt-5 mb-5 justify-content-center">
        <div class="col col-lg-8 ">
            <h1 class="display-4 animate__animated animate__zoomInDown animate__delay-1s">
                <span class="badge badge-transparent manual_title">Manual de uso</span>
            </h1>
            <p class="lead animate__animated animate__zoomInDown animate__delay-1s manual_description">
                <b>En este vídeo descubrirás como usar la herramienta, y así crear tu propia historia de paz.</b>
            </p>
            <div class="embed-responsive embed-responsive-16by9 mt-3">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ecR8JigspQ4?rel=0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>

