<style>
    html,
    body {
        height: 100%;
    }

    body {
        background-image: url("assets/images/TexturaColmenaClaro.jpg");
    }
</style>


<div class="sticky-top">
    <a href="?c=home">
        <img src="assets\images\Home boton.png" alt="Home" width="72" height="72" title="Inicio">
    </a>
</div>


<div class="container">
    <div class="row">
        <div class="col col-lg-6">
            <div class="row text-center">
                <div class="col">
                    <a target="_blank" href="http://files.unicef.org/argentina/spanish/ar_insumos_educvaescuela4.PDF">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-png" src="assets/images/Tarro_1.png">
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a target="_blank" href="https://www.unicef.org/ecuador/media/746/file/El%20Tesoro%20de%20Pazita.pdf">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-png" src="assets/images/Tarro_2.png">
                        </div>
                    </a>
                </div>
            </div>
            <div class="row mt-5 mb-5 text-center">
                <div class="col">
                    <a target="_blank" href="http://ocupas.mx/wp-content/uploads/2020/03/Glosario_Ilustrado_de_la_Cultura_v4.pdf">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-png" src="assets/images/Tarro_3.png">
                        </div>
                    </a>
                </div>

                <div class="col">
                    <a target="_blank" href="https://tzibalnaah.unah.edu.hn/handle/123456789/10027">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-png" src="assets/images/Tarro_4.png">
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col offset-lg-1 col-lg-5">
            <div class="row">
                <div class="col col-lg-2">
                    <div class="animate__animated animate__flipInX">
                        <div class="container_message">
                            <img class="icon_inspiration" src="assets/images/boton2.png" alt="boton">
                            <h1 class="justify-content-center display-6 title_message animate__animated animate__zoomInDown animate__delay-1s">
                                <span class="badge" style="font-size:100%; white-space:normal">Saber más</span>
                            </h1>
                            <p class="lead animate__animated text_message animate__flipInY animate__delay-1s">
                                Aquí vas a enterarte de otros sitios web, los cuales tienen cosas muy interesante para que conozcas sobre la cultura de paz.
                            </p>
                        </div>
                    </div>
                    <img class="irene_message animate__animated animate__pulse animate__infinite" style="height: 350px" src="assets/images/eirene.png">
                </div>
            </div>
        </div>
    </div>
</div>