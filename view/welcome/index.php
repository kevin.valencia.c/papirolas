<style>
    html,
    body {
        height: 100%;
        font-family: 'Raleway', sans-serif;
    }

    body {
        background-image: url("assets/images/paginaInicio/escenaInicioSinboton.jpg");
        background-position: center;
    }

    .background-world {
        position: relative;

    }
</style>

<div class="container h-100 background-world">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col col-lg-8  text-center">
            <div class="jumbotron bg-transparent animate__animated animate__flipInX">
                <h1 class="display-4 title_welcome animate__animated animate__zoomInDown animate__delay-1s">
                    La paz en las colmenas
                </h1>
                <h3 class="animate__animated animate__zoomInDown animate__delay-1s subtitle_welcome">
                    Acompaña a Eirene en esta aventura.
                </h3>
                <br>
                <p class="animate__animated animate__backInDown animate__delay-2s">
                    <a href="?c=home">
                        <img class="button_size" src="assets/images/botonVamos.png" alt="Vámos">
                    </a>
                </p>
            </div>
        </div>
        <div class="col  text-center">
            <img class="animate__animated animate__pulse animate__infinite" style="height: 350px" src="assets/images/eirene.png">
        </div>
    </div>
</div>