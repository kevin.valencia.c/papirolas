<style xmlns="http://www.w3.org/1999/html">
    html,
    body {
        height: 100%;
    }

    body {
        background-image: url("assets/images/TexturaColmenaClaro.jpg");
    }
</style>

<div class="sticky-top">
    <a href="?c=home">
        <img src="assets\images\Home boton.png" alt="Home" width="72" height="72" title="Inicio">
    </a>
</div>


<div class="container mt-5">
    <div class="row">
        <div class="col">
            <div class="jumbotron bg-light">
                <img src="assets/images/udg.png" class="img-fluid" alt="Responsive image">
                <p class="mt-5">
                    <b>Dra. María Esther Avelar Álvarez</b>, Rectora del Sistema de Universidad Virtual de la Universidad de Guadalajara
                </p>
                <p>
                    <b>Mtra. María del Consuelo Delgado González</b>, Directora Administrativa de UDGVirtual
                </p>
                <p>
                    <b>Dr. Jorge Alberto Balpuesta Pérez</b>, Director Académico de UDGVirtual
                </p>
                <p>
                    <b>Mtro. Gladstone Oliva Iñiguez</b>, Director de Tecnologías de UDGVirtual
                </p>
                <p class="lead mt-5 mb-5">
                    Agradecemos a cada uno de los niños y las niñas que participaron en la grabación de los vídeos.
                </p>

                <h4 class="my-4">Organización técnica</h4>
                <h5>Programa de Extensión de UDGVirtual</h5>

                <ul>
                    <li>Dra. Rosa Noemí Moreno Ramos</li>
                    <li>Lic. María Isabel Cueva Flores</li>
                </ul>
                <h5>Programa de Innovación de UDGVirtual</h5>
                <ul>
                    <li>Mtra. Guadalupe Jeanette González Díaz</li>
                    <li>Lic. Ana Karen Mercado Castellanos</li>
                    <li>Lic. Brandon Iván Velázquez Santana</li>
                </ul>
                <h5>Equipo de desarrollo</h5>
                <ul>
                    <li>Kevín Arturo Valencia Castañeda - Líder técnico</li>
                    <li>Juan Pedro Martinez García - PM</li>
                    <li>Israel Martínez Jiménez</li>
                    <li>Antonio Rodríguez Barrientos</li>
                    <li>Enrique Mena García</li>
                </ul>
                <h4 class="my-4">Coordinación Académica</h4>
                <h5>Instituto de Gestión del Conocimiento y del Aprendizaje en Ambientes Virtuales de UDGVirtual</h5>
                <ul>
                    <li>Dra. Dolores del Carmen Chinas Salazar</li>
                    <li>Dra. María Elena Chan Nuñez</li>
                </ul>
                <h4 class="my-4">Recursos gráficos y producción de vídeos</h4>
                <h5>Coordinación de Producción de UDGVirtual</h5>
                <ul>
                    <li>Mtro. Abraham Andrade López</li>
                    <li>Diseñador Ebert Enoc Guerra Torres</li>
                    <li>Lourdes Ivett Covarrubias Trevino</li>
                </ul>
                <h4 class="my-4">Soporte técnico</h4>
                <h5>Coordinación de Soporte de UDGVirtual</h5>
                <ul>
                    <li>Mtro. Carlos Alejandro Sánchez Ramírez</li>
                </ul>
                <h4 class="my-4">Comunicación y redes sociales</h4>
                <h5>Programa de comunicación y difusión de UDGVirtual</h5>
                <ul>
                    <li>Mtra. Nancy Wendy Aceves Velázquez</li>
                    <li>Lic. Brenda Alvarado Flores - redes sociales</li>
                    <li>Lic. Liliana Martínez Gómez - sitio web</li>
                    <li>Lic. Karina Alatorre Oliva - noticias</li>
                </ul>
                <h5>Grabación de vídeos</h5>
                <ul>
                    <li>Mario René Yamá Manrique</li>
                    <li>Yolanda Hernández Frutos</li>
                </ul>
            </div>
        </div>
    </div>
</div>