<style>
    body {
        background-image: url("assets/images/TexturaColmenaClaro.jpg");
   }
</style>


<div class="sticky-top">
    <a href="?c=Opinions">
        <img src="assets\images\Regresar boton.png" alt="Back" width="72" height="72" title="Atras">  
    </a>
    <a href="?c=home">
        <img src="assets\images\Home boton.png" alt="Home" width="72" height="72" title="Inicio">
    </a>
</div>

<div class="container">
    <div class="row mt-5 justify-content-center">
        <div class="col col-lg-8 ">
            <h1 class="display-4 animate__animated animate__zoomInDown animate__delay-1s">
                <span class="badge badge-info bg-transparent title-valores">Cultura Paz</span>
            </h1>
                <iframe width="800" height="600" src="https://www.youtube.com/embed/chgwnK06Lv0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>

<div class="fixed-bottom">
    <a href="?c=opinions&a=Empatia" class="float-right">
        <img src="assets\images\boton adelante.png" alt="Next" width="72" height="72" title="Siguiente">
    </a>
</div>