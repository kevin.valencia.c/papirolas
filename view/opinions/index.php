<style>
    html,
    body {
        height: 100%;
    }

    body {
        background-image: url("assets/images/TexturaColmenaClaro.jpg");
    }

</style>


<div class="sticky-top">
    <a href="?c=home">
        <img src="assets\images\Home boton.png" alt="Home" width="72" height="72" title="Inicio">
    </a>
</div>


<div class="container">
    <div class="row">
        <div class="col col-lg-6">
            <div class="row text-center">
                <div class="col">
                <a href="?c=opinions&a=CulturaPaz">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-svg animate__animated animate__pulse animate__infinite" src="assets/images/opinamos/CulturaPaz.png" alt="Cultura Paz" width="200" height="200" title="Cultura Paz"></img>
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a href="?c=opinions&a=Honestidad">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-svg animate__animated animate__pulse animate__infinite" src="assets/images/opinamos/Honestidad.png" alt="Empatia" width="200" height="200" title="Honestidad"></img>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row mt-5 mb-5 text-center">
                <div class="col">
                    <a href="?c=opinions&a=Igualdad">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-svg animate__animated animate__pulse animate__infinite" src="assets/images/opinamos/Igualdad.png" alt="Igualdad" width="200" height="200" title="Igualdad"></img>
                        </div>
                    </a>
                </div>

                <div class="col">
                    <a href="?c=opinions&a=Inclusion">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-svg animate__animated animate__pulse animate__infinite" src="assets/images/opinamos/Inclusion.png" alt="Inclusion" width="200" height="200" Title="Inclusión"></img>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row mt-5 mb-5 text-center">
                <div class="col">
                    <a href="?c=opinions&a=Justicia">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-svg animate__animated animate__pulse animate__infinite" src="assets/images/opinamos/Justicia.png" alt="Justicia" width="200" height="200" title="Justicia"></img>
                        </div>
                    </a>
                </div>
                <div class="col">
                    <a href="?c=opinions&a=Libertad">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-svg animate__animated animate__pulse animate__infinite" src="assets/images/opinamos/Libertad.png" alt="Libertad" width="200" height="200" title="Libertad"></img>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row mt-5 mb-5 text-center">
                <div class="col">
                    <a href="?c=opinions&a=Respeto">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-svg animate__animated animate__pulse animate__infinite" src="assets/images/opinamos/Respeto.png" alt="Respeto" width="200" height="200" title="Respeto"></img>
                        </div>
                    </a>
                </div>
                <div class="col">
                <a href="?c=opinions&a=Empatia">
                        <div class="img-overlay-wrap">
                            <img class="img-overlay-wrap-svg animate__animated animate__pulse animate__infinite" src="assets/images/opinamos/Empatia.png" alt="Empatia" width="200" height="200" title="Empatia"></img>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="col offset-lg-1 col-lg-5">
            <div class="row">
                <div class="col col-lg-2">
                    <div class="animate__animated animate__flipInX">
                        <div class="container_message">
                        <img class="icon_inspiration" src="assets/images/boton2.png" alt="boton">
                            <h1 class="display-6 title_message animate__animated animate__zoomInDown animate__delay-1s">
                                <span class="badge" style="font-size:100%; white-space:normal"> Las niñas y los niños opinamos</span>
                            </h1>
                            <p class="lead animate__animated text_message animate__flipInY animate__delay-1s">
                                En esta colmena podrás ver niñas y niños como tú, que te explicarán los valores de la cultura de
                                paz. ¿Con cuál te identifcas más?
                            </p>
                        </div>
                    </div>
                    <img class="irene_message animate__animated animate__pulse animate__infinite" style="height: 350px" src="assets/images/opinamos/opinando.png">
                </div>
            </div>
        </div>
    </div>
</div>