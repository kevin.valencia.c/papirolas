<style>
    html,
    body {
        height: 100%;
    }

    body {
        background-image: url("assets/images/escenaPrincipal/backgroundHome.png"), url("assets/images/texturaColmena.jpg");
        background-repeat: no-repeat, repeat;
        background-position: top;

        /* background-image: url("assets/images/escenaPrincipal/BGcolmenas.png"), url(assets/images/escenaPrincipal/);
        background-repeat: no-repeat;
        background-position: center;
        background-size: 1920px 1242px; */
    }

    @media only screen and (max-width: 1930px) {
        body {
            background-size: 1700px auto, cover;
        }
    }

    @media only screen and (max-width: 1370px) {
        body {
            background-size: 1100px auto, cover;
        }
    }

    @media only screen and (max-width: 992px) {
        body {
            background-size: 792px auto, cover;
        }
    }

    @media only screen and (max-width: 768px) {
        body {
            background-size: 568px auto, cover;
        }
    }

    @media only screen and (max-width: 576x) {
        body {
            background-size: 376px auto, cover;
        }
    }
</style>
<div class="container-fluid">
    
</div>
<div class="title">
        Ingresa a cada una de las colmenas...
    </div>
    <div class="margin_anchor">
    </div>
    <a href="?c=opinions">
        <div class="hive_1">
        </div>
    </a>
    <a href="?c=maker">
        <div class="hive_2">
        </div>
    </a>
    <a href="?c=credits">
        <div class="credits">
        </div>
    </a>
    <a href="?c=more">
        <img class="more" src="assets/images/escenaPrincipal/abejaTarro.png" alt="Saber Más">
    </a>

<!-- <div>
    <div class="img-wrap">
        <a href="#">
            <img class="hive_1" src="assets/images/escenaPrincipal/colmena_1.png" alt="colmena1">
        </a>
    </div>
    <div>
        <a href="#">
            <img class="hive_2" src="assets/images/escenaPrincipal/colmena_2.png" alt="colmena2">
        </a>
    </div>
    <div>
        <a href="#">
            <img class="credits" src="assets/images/escenaPrincipal/colmena_creditos.png" alt="créditos">
        </a>
    </div>
</div> -->
<!--
<div class="container fill">
    <div class="row justify-content-md-center align-items-center">
        <div class="col col-lg-6 mt-5 mb-auto text-center">
            <div class="jumbotron bg-transparent bg-warning animate__animated animate__flipInX">
            <h1 class="display-4">La paz en las colmenas</h1>
            <h3>Acompaña a Eirene en esta aventura.</h3>
            <p class="lead">
                <a class="btn btn-light btn-lg" href="#" role="button">Vamos</a>
            </p>
            </div>

        </div>
    </div>

    <div class="row justify-content-lg-end">
        <div class="col col-lg-3">
            <img class="animate__animated animate__pulse animate__infinite" style="height: 350px"
                 src="assets/images/eirene.png">
        </div>
    </div>
</div>
-->
<!-- sin comentar -->
<!-- <div class="container-fluid h-100">
    <div class="row mt-5 mb-5">
        <div class="col text-center mt-5">
            <h3 class="display-3 mt-5 animate__animated animate__zoomInDown title">
                ¡Sigue el camino de las colmenas!
            </h3>
        </div>
    </div>
    <div class="row justify-content-center align-items-center mt-5 text-center"> -->
<!-- fin sincomentar -->
<!--
        <div class="col mt-5">
            <a href="?c=peace">
                <img class="animate__animated animate__pulse animate__infinite" style="max-height: 200px"
                     src="assets/images/hive.png">
            </a>
            <h3 class="display-4 name-hive">
                COLMENA PAZ
            </h3>
        </div>
        -->
<!-- sin comentar -->
<!-- <div class="col col-lg-4 mt-5">
            <a href="?c=opinions">
                <img class="animate__animated animate__pulse animate__infinite" style="max-height: 200px" src="assets/images/hive.png">
            </a>
            <h3 class="display-4 name-hive">
                LAS NIÑAS Y LOS NIÑOS OPINAMOS
            </h3>
        </div>
        <div class="col col-lg-4 mt-5">
            <a href="?c=maker">
                <img class="animate__animated animate__pulse animate__infinite" style="max-height: 200px" src="assets/images/hive.png">
            </a>
            <h3 class="display-4 name-hive">
                CREADORES
            </h3>
        </div>
    </div>
    <div class="row justify-content-end align-items-center mt-5 mb-5 text-center">
        <div class="col col-lg-3">
            <a href="?c=more">
                <img class="animate__animated animate__pulse animate__infinite" style="max-height: 150px" src="assets/images/vase.png">
                <h3 class="name-hive">
                    PARA SABER MÁS
                </h3>
            </a>
        </div>
    </div>
</div> -->